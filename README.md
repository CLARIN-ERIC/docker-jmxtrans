Image for [jmxtrans](http://www.jmxtrans.org) developed for usage in CLARIN environments.

## Usage

Usage is roughly the same as the 
[official jmxtrans image](https://github.com/jmxtrans/jmxtrans/tree/master/jmxtrans/docker),
except that this image is using supervisor and does *not* accept the `start-with-jmx` and
`start-without-jmx` options.

```sh
docker run -d -v `pwd`/json-files:/var/lib/jmxtrans -P registry.gitlab.com/clarin-eric/docker-jmxtrans:1.0.0 
docker run -d -v `pwd`/json-files:/var/lib/jmxtrans -e START_WITH_JMX=true -p 9999:9999 registry.gitlab.com/clarin-eric/docker-jmxtrans:1.0.0
```

See the [official documentation](https://github.com/jmxtrans/jmxtrans/wiki) for 
information on configuring jmxtrans through json files.
